import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

@Named
@RequestScoped
public class OrderBean {

    private static final Order[] orders = new Order[] {
            new Order("123", "Order1"),
            new Order("234", "Order2"),
            new Order("345", "Order3"),
            new Order("456", "Order4")
    };

    public Order[] getOrders() {
        return orders;
    }

    public String editAction(Order order) {
        order.setEditable(true);
        return null;
    }

    public String saveAction(Order order) {
        order.setEditable(false);
        return null;
    }
}
